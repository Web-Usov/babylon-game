class Game {
    /**
     * Start
     */
    public start() {
        console.log("Start game!")
    }

    /**
     * end
     */
    public end() {
        console.log("End game.")
    }
}

export default Game;